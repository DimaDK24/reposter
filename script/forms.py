from django.forms import ModelForm

from script.models import Template


class TemplateForm(ModelForm):
    class Meta:
        model = Template
        fields = ('name', 'photos', 'posts', 'publics', 'texts', 'times')

