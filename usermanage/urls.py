from django.urls import path
from django.contrib.auth.urls import views as auth_views
from django.views.generic import RedirectView

from usermanage import views

app_name = 'usermanage'
urlpatterns = [
    path('reg', views.Reg.as_view(), name='reg'),
    path('login', auth_views.LoginView.as_view(redirect_authenticated_user=True), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    path('change_password', auth_views.PasswordChangeView.as_view(), name='change_password'),
    path('change_password/done', auth_views.PasswordChangeDoneView.as_view(), name='change_password_done'),
    path('', RedirectView.as_view(pattern_name='script:add_post'))
]
