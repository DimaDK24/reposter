from django.contrib import admin

# Register your models here.
from script import models
from script.models import Type


class TemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'pk', 'created', 'active', 'user')
    list_filter = ['user', 'active', 'user', 'created']
    search_fields = ['name', 'publics', 'pk']
    list_display_links = ['pk', 'name']
    list_select_related = ['user']
    readonly_fields = ['created']


class PostAdmin(admin.ModelAdmin):
    list_display = ('public', 'pk', 'link', 'datetime', 'attempts', 'type', 'user')
    list_filter = ['public', 'type', 'user', 'attempts', 'datetime', 'created']
    search_fields = ['public', 'link']
    list_display_links = ['public', 'pk', 'link']
    list_select_related = ['type', 'user']
    list_editable = ['attempts', 'type']
    actions = ['set_added', 'set_error']
    readonly_fields = ['created']

    def set_error(self, request, queryset):
        rows_updated = queryset.update(type=Type.objects.get(text='Error'))
        self.message_user(request, f'{rows_updated} постов помечены как ошибочные')

    set_error.short_description = 'Пометить выбранные посты как ошибочные'

    def set_added(self, request, queryset):
        rows_updated = queryset.update(type=Type.objects.get(text='Added'), attempts=0)
        self.message_user(request, f'{rows_updated} постов помечены как добавленные')

    set_added.short_description = 'Поменить выбранные посты как добавленные'


admin.site.register(models.Template, TemplateAdmin)
admin.site.register(models.Post, PostAdmin)
admin.site.register(models.Info)
admin.site.register(models.Type)
admin.site.register(models.NotifyType)
