from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
from django.utils import timezone


class Post(models.Model):
    link = models.URLField()
    public = models.CharField(max_length=40)
    text = models.TextField()
    image = models.URLField()
    datetime = models.DateTimeField()
    created = models.DateTimeField()
    new_post_id = models.CharField(max_length=40, blank=True)
    type = models.ForeignKey('Type', on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, db_index=True)
    attempts = models.PositiveSmallIntegerField(default=0)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.pk:
            self.created = timezone.now()
        super().save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return self.link


class Template(models.Model):
    name = models.CharField(max_length=80, verbose_name='Имя')
    photos = models.TextField(verbose_name='Ссылки на изображения', blank=True)
    posts = models.TextField(verbose_name='Ссылки на посты', blank=True)
    publics = models.TextField(verbose_name='Паблики', blank=True)
    texts = models.TextField(verbose_name='Тексты', blank=True)
    times = models.TextField(verbose_name='Время постов', blank=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, db_index=True)
    order_id = models.PositiveIntegerField()
    created = models.DateTimeField()
    active = models.BooleanField(default=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.pk:
            self.created = timezone.now()
        if not self.order_id:
            last = self.__class__.objects.all().last()
            self.order_id = self.pk or (last and last.pk + 1) or 1
        super().save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return self.name


class Info(models.Model):
    vk_id = models.BigIntegerField(primary_key=True)
    access_token = models.CharField(max_length=200)
    tg_id = models.BigIntegerField(blank=True, null=True)
    notify_type = models.ForeignKey('NotifyType', on_delete=models.CASCADE, default=1)
    profile_dir = models.CharField(max_length=20, blank=True)
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, db_index=True)

    def __str__(self):
        return str(self.user)


class Type(models.Model):
    text = models.CharField(max_length=15)

    def __str__(self):
        return self.text


class NotifyType(models.Model):
    text = models.CharField(max_length=15)

    def __str__(self):
        return self.text
