from django.urls import path

from . import views

app_name = 'script'
urlpatterns = [
    path('templates/create', views.CreateTemplateView.as_view(), name='create_template'),
    path('templates/<int:template_id>', views.UpdateTemplateView.as_view(), name='template_detail'),
    path('templates/<int:template_id>/copy', views.CopyTemplateView.as_view(), name='copy_template'),
    path('templates/updateOrderApi', views.UpdateTemplateOrderApi.as_view(),
         name='update_template_order_api'),
    path('templates/getByIdApi', views.GetTemplateByIdApi.as_view(), name='get_template_by_id_api'),
    path('templates/deleteApi', views.DeleteTemplateApi.as_view(), name='delete_template_api'),
    path('templates/', views.ListTemplatesView.as_view(), name='list_templates'),
    path('post/<int:post_id>', views.UpdatePostView.as_view(), name='update_post'),
    path('post/updateApi', views.UpdatePostApi.as_view(), name='update_post_api'),
    path('post/add', views.AddPostView.as_view(), name='add_post'),
    path('post/addApi', views.AddPostApi.as_view(), name='add_post_api'),
    path('settings', views.Settings.as_view(), name='settings'),
    path('vk_login', views.VKRedirectURI.as_view(), name='vk_redirect_uri'),
    path('tg_api', views.TGApi.as_view(), name='tg_api'),
    path('save_api', views.SaveApi.as_view(), name='save_api'),
    path('', views.IndexView.as_view(), name='index')
]
