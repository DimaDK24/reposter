import re
from datetime import datetime
from typing import BinaryIO

import requests

from vk_api import VkRequest


def is_article_link(link: str) -> bool:
    return is_production_article_link(link) or is_edit_article_link(link)


def is_production_article_link(link: str) -> bool:
    beginnings = ['https://vk.com/@', 'http://vk.com/@', 'vk.com/@']
    for beginning in beginnings:
        if beginning in link:
            return True
    return False


def is_edit_article_link(link: str) -> bool:
    return bool(re.search('z=article_edit-?\d+_\d+$', link))


def get_article_id_from_edit_link(link: str) -> str:
    article_id = link.split('z=article_edit')[1]
    return f'article{article_id}'


def convert_date_to_timestamp(date: datetime) -> int:
    return int(date.timestamp())


def create_attachments(article: str, image: str) -> str:
    return f'{article},{image}'


def upload_image(access_token, file_content: BinaryIO, group_id) -> str:
    r = VkRequest(access_token, '5.92')
    upload_url = r('photos.getWallUploadServer', group_id=group_id)['upload_url']
    files = {'photo': file_content}
    response = requests.post(upload_url, files=files).json()
    save_response = r('photos.saveWallPhoto', group_id=group_id,
                      photo=response['photo'], server=response['server'], hash=response['hash'])
    uploaded_photo = save_response[0]
    return f'photo{uploaded_photo["owner_id"]}_{uploaded_photo["id"]}'


def post_wall_post(owner_id: int, message: str,
                   attachments: str, date: int,
                   access_token: str) -> int:
    r = VkRequest(access_token, '5.92')
    post_id = r('wall.post', owner_id=owner_id, message=message,
                attachments=attachments, publish_date=date)['post_id']
    return post_id


def post_wall_post_with_article(access_token: str, owner_id: int,
                                message: str, image_path: str,
                                article_id: str, date: datetime) -> str:
    if image_path:
        with open(image_path, 'rb') as file:
            image_id = upload_image(access_token, file, owner_id)
        attachments = create_attachments(article_id, image_id)
    else:
        attachments = article_id
    timestamp = convert_date_to_timestamp(date)
    post_id = post_wall_post(-owner_id, message, attachments, timestamp, access_token)
    return f'{owner_id}_{post_id}'


class VkArticlePageChange(Exception):
    pass


def get_article_id_from_production_link(link: str) -> str:
    if not is_with_protocol(link):
        link = f'http://{link}'
    page = requests.get(link).text
    start_search_string = '"article_view_'
    if start_search_string not in page:
        raise VkArticlePageChange
    after_start = page[page.index(start_search_string) + len(start_search_string):]
    article_id = after_start[:after_start.index('"')]
    if not re.match('-?\d+_\d+$', article_id):
        raise VkArticlePageChange
    return f'article{article_id}'


def is_with_protocol(link: str) -> bool:
    return link.startswith('https://') or link.startswith('http://')


class BadArticleLink(Exception):
    pass


def convert_article_link_to_id(link: str) -> str:
    if is_edit_article_link(link):
        return get_article_id_from_edit_link(link)
    if is_production_article_link(link):
        return get_article_id_from_production_link(link)
    raise BadArticleLink
