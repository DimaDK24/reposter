from typing import Any

import requests


class TgException(Exception):
    pass


class TgRequest:
    def __init__(self, bot_token):
        self.api_url = 'https://api.telegram.org/bot' + bot_token + '/'

    def __call__(self, method, **kwargs):
        response = self._perform_request(self.api_url + method, data=kwargs).json()
        self._check_response(response)
        return response['result']

    def _check_response(self, response):
        if not response['ok']:
            error_desc = response.get('description')
            raise TgException(f'TG error: {error_desc}.')

    def _perform_request(self, url: str, data: dict) -> Any:
        response = self._low_level_perform_request(url, data)
        self._check_response_code(response.status_code)
        return response

    def _low_level_perform_request(self, url, data):
        try:
            return requests.post(url, data=data)
        except requests.RequestException as e:
            raise TgException(f'Error with TG connection: {str(e)}.')

    def _check_response_code(self, code: int):
        if not code == requests.codes.ok:
            raise TgException(f'Error with TG response code ({str(code)}).')
