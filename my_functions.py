from datetime import timedelta, timezone
from typing import Dict, List, Tuple

import logging
import os
import re
import requests
import shutil

from models import Post, Type
from my_exceptions import BadPostLink, BadUserName
from vk_api import VkRequest

logger = logging.getLogger(__name__)


def get_admin_pages(uid: int, access_token: str) -> Dict[str, int]:
    r = VkRequest(access_token, 5.74)
    items = r('groups.get', user_id=uid, filter='moder', extended=1)['items']
    names = {}
    for item in items:
        names.update({item['name'].casefold(): item['id']})
    return names


def find_public(public_name: str, user_publics: Dict) -> int:
    public_name = public_name.casefold()
    for (key, value) in iter(user_publics.items()):
        if public_name in key:
            return value
    raise KeyError


def rstrip_list(text_list: List[str]) -> List[str]:
    new_list = []
    for item in text_list:
        item = item.rstrip()
        new_list.append(item)
    return new_list


def remove_spaces(text: str) -> str:
    new_text = ''
    for symbol in text:
        if symbol not in (' ', '\t', '\n', '\r') and symbol.isalpha():
            new_text += symbol
    return new_text


def strip_equals(text1: str, text2: str) -> bool:
    lines1 = rstrip_list(remove_spaces(text1).splitlines())
    lines2 = rstrip_list(remove_spaces(text2).splitlines())
    return lines1 == lines2


def check_post(post: Post, public_id: int, access_token: str) -> Tuple[bool, str]:
    r = VkRequest(access_token, 5.74)
    logger.debug('Checking post: %s, datetime: %s', post.get_id(), post.datetime.strftime('%d.%m.%Y %H:%M'))
    public_posts = r('wall.get', owner_id='-' + str(public_id), count=100, filter='postponed')['items']
    post_datetime = int(post.datetime.timestamp())
    for public_post in public_posts:
        if strip_equals(convert_vk_link(post.text), public_post['text']) and post_datetime == public_post['date']:
            try:
                attachments = public_post['attachments']
            except KeyError:
                continue
            if not attachments[0]['type'] == 'photo':
                continue
            try:
                reposted_post = public_post['copy_history']
            except KeyError:
                continue
            try:
                post_repost_owner_id = int(post.link[post.link.rindex('wall') + len('wall'): post.link.rindex('_')])
                post_repost_post_id = int(post.link[post.link.rindex('_') + 1:])
            except ValueError:
                raise BadPostLink('Неверная ссылка не должна доходить до проверки поста')
            if not (post_repost_post_id == reposted_post[0]['id']
                    and post_repost_owner_id == reposted_post[0]['owner_id']):
                continue
            return True, f"{public_post['owner_id']}_{public_post['id']}"
    return False, public_posts


def write_data_to_file(filename: str, text: str, sikulix_dir: str) -> None:
    with open(sikulix_dir + filename, 'w', encoding='utf8') as f:
        f.write(text)


def write_data_to_files(post: Post, sikulix_dir: str, user_profile_dir: str) -> None:
    write_data_to_file('link.txt', post.link, sikulix_dir)
    write_data_to_file('public.txt', post.public, sikulix_dir)
    write_data_to_file('text.txt', post.text, sikulix_dir)
    write_data_to_file('person.txt', user_profile_dir, sikulix_dir)
    logger.debug('Writing post: %s, datetime: %s', post.get_id(), post.datetime.strftime('%d.%m.%Y %H:%M'))
    write_data_to_file('time.txt', post.datetime.strftime('%d.%m.%Y %H:%M'), sikulix_dir)


def load_image(link: str, directory: str) -> str:
    if not (link.startswith('http://') or link.startswith('https://')):
        link = 'http://' + link
    try:
        r = requests.get(link, timeout=10)
        if not r.status_code == requests.codes.ok:
            raise requests.RequestException()
    except requests.RequestException:
        raise requests.RequestException('Не удалось загрузить картинку')
    if directory:
        shutil.rmtree(directory)
        os.mkdir(directory)
    extension = get_extension(link)
    i = 0
    while True:
        filepath = directory + str(i) + extension
        if not os.path.exists(filepath):
            break
        else:
            i += 1
    with open(filepath, 'wb') as f:
        f.write(r.content)
    return filepath


class BadExtension(Exception):
    pass


def get_extension(filename: str) -> str:
    extension = filename[filename.rfind('.'):]
    extension_formats = ['.jpg', '.png', '.gif', '.webp']
    for extension_format in extension_formats:
        if extension_format in extension:
            return extension_format
    raise BadExtension(f'Couldn\'t resolve extension of file: {filename}. '
                       f'Allowed extensions: f{extension_formats}.')


def _re_vk_link_converter(match_obj) -> str:
    link_id = match_obj.group(1)
    link_text = match_obj.group(2)
    try:
        link_id = resolve_link(link_id)
    except BadUserName:
        return match_obj.group(0)
    return f'[{link_id}|{link_text}]'


def convert_vk_link(text: str) -> str:
    return re.sub(r'[@*](\S+)[ ]*\((.+?)\)', _re_vk_link_converter, text)


def resolve_link(string: str) -> str:
    if re.match('^(club|page)\d+$', string):
        return string
    else:
        # сервисный ключ доступа приложения
        r = VkRequest(access_token='8974fb568974fb568974fb56708916ad73889748974fb56d3b92ad5034f2fe3cb9689b2',
                      version=5.76)
        response = r('utils.resolveScreenName', screen_name=string)
        if 'type' not in response or response['type'] not in ['group', 'page', 'club']:
            raise BadUserName()
        return 'club' + str(response['object_id'])


def read_from_file(filename: str) -> str:
    with open(filename, encoding='utf8') as f:
        return f.read()


def file_exists(filename: str) -> bool:
    return os.path.isfile(filename)


def clean_chrome_cache():
    cache_dir = os.environ.get('CHROME_CACHE_DIR')
    if cache_dir and os.path.isdir(cache_dir):
        shutil.rmtree(cache_dir)


def count_posts(last_post: Post, posts_type: Type) -> int:
    return (Post.select().where(
        (Post.type == posts_type) &
        (Post.created > last_post.created - timedelta(minutes=20)) &
        (Post.user == last_post.user)
    ).count())


def get_posts_number(last_post: Post, post_types: List[str]) -> List[int]:
    post_number = []
    for post_type in post_types:
        post_number.append(count_posts(last_post, Type.get(Type.text == post_type)))
    return post_number


header_text = "Я завершил все репосты"
success_only_report_format = "Все посты прошли *успешно* 😊"
error_only_report_format = "Все посты прошли *ошибочно* 😔"
both_report_format = ("Прошло постов *успешно*: {success_number}\n"
                      "Прошло постов *ошибочно*: {error_number}")
footer_text = "Всего их было: {all_number}"


def get_main_text(success: int, error: int) -> str:
    if success == 0 and error > 0:
        return error_only_report_format
    elif error == 0 and success > 0:
        return success_only_report_format
    elif error > 0 and success > 0:
        return both_report_format.format(success_number=success,
                                         error_number=error)
    else:
        return "Странная ошибка с подсчетом постов"


def create_report_text(post_numbers: List[int]) -> str:
    main_text = get_main_text(*post_numbers)
    footer = footer_text.format(all_number=sum(post_numbers))
    return "\n".join((header_text, main_text, footer))


def get_owner_id_from_link(link: str) -> int:
    return int(link[link.index('wall') + 4: link.index('_')])


def create_post_text(post: Post) -> str:
    title = post.text.splitlines()[0]
    text_minsk_datetime = post.datetime.astimezone(timezone(timedelta(hours=3))).strftime("%d.%m %H:%M")
    return f'*{title}* в *{post.public}* *({text_minsk_datetime})*'
