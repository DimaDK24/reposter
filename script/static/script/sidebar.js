$(document).ready(function () {
    window.sidebar_buttons = false;
    // function setSidebarDisplay() {
    //     const width = $(window).width();
    //     if (width < 1040 && !window.sidebar_buttons){
    //         const close_but = '<button id="popup-close-button-wrapper">\n' +
    //             '                <i class="fas fa-times"></i>\n' +
    //             '            </button>';
    //         const menu_but = '<button id="menu">\n' +
    //             '            <i class="fas fa-bars"></i>\n' +
    //             '        </button>';
    //         $('div.popup-header-wrapper').append(close_but);
    //         $('div.content > div.header-wrapper').prepend(menu_but);
    //         window.sidebar_buttons = true;
    //     } else if (width > 1040 && window.sidebar_buttons){
    //         $('button#popup-close-button-wrapper').remove();
    //         $('button#menu').remove();
    //     }
    // }

    function toggleSidebarPopup() {
        const nav = $('nav');
        nav.toggleClass('popup');
        const wrapper = nav.parent();
        wrapper.toggleClass('popup-wrapper');
        const body = $('body');
        body.toggleClass('popup-show');
    }

    // setSidebarDisplay();

    $('#menu').click(function () {
        toggleSidebarPopup();
    });

    $(document).on('click', 'nav', function (e) {
        e.stopPropagation();
    });

    $(document).on('click', 'div.popup-wrapper, button#popup-close-button-wrapper', function () {
        toggleSidebarPopup();
    });

    $(window).on('resize', function () {
        // setSidebarDisplay();
    });
});
