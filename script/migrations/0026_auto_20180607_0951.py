# Generated by Django 2.0.6 on 2018-06-07 06:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('script', '0025_post_attemps'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='attemps',
            new_name='attempts',
        ),
    ]
