$(document).ready(function () {
    new ClipboardJS('button#copy');
    $('select#notify_mode_select').select2({
        language: "ru"
    });
    $('#check').click(function () {
        const csrftoken = Cookies.get('csrftoken');
        $.ajax({
            url: "/app/tg_api",
            method: "POST",
            success: function (data) {
                if (data.ok)
                    swal({
                        type: 'success',
                        title: 'Успешно',
                        text: 'Подключен бот'
                    });
                else
                    swal({
                        type: 'error',
                        title: 'Ошибка',
                        text: data.error
                    })
            },
            error: function(){
                swal({
                    type: 'error',
                    title: 'Ошибка',
                    text: 'Возникла внутреняя ошибка сервера'
                })
            },
            headers: {
                "X-CSRFToken": csrftoken
            }
        });
    });
    $('#save').click(function () {
        const csrftoken = Cookies.get('csrftoken');
        const number = $('#notify_mode_select').val();
        $.ajax({
            url: "/app/save_api",
            method: "POST",
            success: function (data) {
                if (data.ok) {
                    swal({
                        type: 'success',
                        title: 'Успешно',
                        text: 'Сохранено'
                    });
                    const notify_type_shower = $('#notify_type_status');
                    let text = '';
                    switch (parseInt(number)){
                        case 0:
                            text = 'Сейчас я не уведомляю ни о чем';
                            break;
                        case 1:
                            text = 'Сейчас я уведомляю только об успешных репостах';
                            break;
                        case 2:
                            text = 'Сейчас я уведомляю только об ошибках';
                            break;
                        case 3:
                            text = 'Сейчас я уведомляю и об успешных репостах, и об ошибках';
                            break;
                    }
                    notify_type_shower.text(text);
                }
                else
                    swal({
                        type: 'error',
                        title: 'Ошибка',
                        text: data.error
                    })
            },
            error: function(){
                swal({
                    type: 'error',
                    title: 'Ошибка',
                    text: 'Возникла внутреняя ошибка сервера'
                })
            },
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: {'number': number}
        });
    });
});
