class ImproperlyConfigured(Exception):
    pass


class BadPublic(Exception):
    pass


class BadTime(Exception):
    pass


class BadPostLink(Exception):
    pass


class BadUserName(Exception):
    pass


class SelfRepost(Exception):
    pass
