$(document).ready(function () {
    $('#delete-but').click(function (e) {
        e.preventDefault();
        $this = $(this);
        const id = $this.data('id');
        const name = $this.data('template-name');
        swal({
            title: 'Ты уверен?',
            html: 'Удалить шаблон <span class="template-name">' + name + "</span>?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да!',
            cancelButtonText: 'Нет, отмена',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                const csrftoken = Cookies.get('csrftoken');
                $.ajax({
                    url: "/app/templates/deleteApi",
                    method: "POST",
                    success: function (data) {
                        if (data.ok) {
                            swal({
                                title: 'Удалено!',
                                html: 'Шаблон <span class="template-name">' + name + "</span> успешно удален",
                                type: 'success'
                            }).then((result) => {
                                location.replace('/app/templates')
                            });
                        }
                        else
                            swal({
                                type: 'error',
                                title: 'Ошибка удаления',
                                html: data.error
                            })
                    },
                    error: function(){
                        swal({
                            type: 'error',
                            title: 'Ошибка',
                            text: 'Возникла внутреняя ошибка сервера'
                        })
                    },
                    headers: {
                        "X-CSRFToken": csrftoken
                    },
                    data: {
                        'number': id
                    }
                });
            }
        })
    });
});