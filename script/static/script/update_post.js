$(document).ready(function () {
    $('button.submit').click(function (e) {
        e.preventDefault();
        const post_id = $('#post_id').text();
        const links = $('#link_area').val().split("\n");
        const publics = $('#public_area').val().split("\n");
        let texts = $('#text_area').val().split("\n_end_\n");
        {
            let end_index = texts[texts.length-1].lastIndexOf("\n_end_");
            if (end_index !== -1)
                texts[texts.length - 1] = texts[texts.length -1].slice(0, end_index);
        }
        if (!texts[texts.length - 1])
            texts.pop();
        const imgs = $('#img_area').val().split("\n");
        const datetimes = $('#datetime_area').val().split("\n");
        const number = links.length;
        if (number > 1 || publics.length > 1 || texts.length > 1 || imgs.length > 1 || datetimes.length > 1) {
            swal({
                type: 'error',
                title: 'Неверно',
                html: `Здесь мы только редактируем пост, а не добавляем несколько. Для этого, просто добавь посты<br>
Здесь: ссылок ${links.length},  пабликов ${publics.length},<br>
текстов ${texts.length}, картинок ${imgs.length} и времени ${datetimes.length}`
            });
            return false;
        }
        for (let i = 0; i < links.length; i++){
            let result = links[i].match(/(wall-|wall)\d+_\d+$/);
            if (result !== null)
                links[i] = 'https://vk.com/' + result[0];
        }

        let data_obj = {};
        let serialize_data = $('form').serializeArray();
        for (let i = 0; i < serialize_data.length; i++){
            data_obj[serialize_data[i].name] = serialize_data[i].value
        }


        const csrftoken = Cookies.get('csrftoken');
        $.ajax({
            url: "/app/post/updateApi",
            method: "POST",
            success: function (data) {
                if (data.ok)
                    swal({
                        type: 'success',
                        title: 'Успешно',
                        text: 'Сохранен и запущен пост'
                    });
                else
                    swal({
                        type: 'error',
                        title: 'Ошибка',
                        html: data.error
                    })
            },
            error: function(){
                swal({
                    type: 'error',
                    title: 'Ошибка',
                    text: 'Возникла внутреняя ошибка сервера'
                })
            },
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: data_obj
        });
    })
});